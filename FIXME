What does work and what doesn't
===============================


Works
-----

- downloading of schedules

- schedule XML parsing

- schedule mining and arranging in a list (one for all the 3 stations)

- logging of "now on air" and "next on air" notifications at the specified
  intervals

- a unit test framework, including tests on dummy episodes

- library structure

- automated build dependency checks

- schedule updates from the client website every midnight with simple retries in
  case of network or processing errors


Doestn't work
-------------

- sending updates to the client hardware

  - sendOnAirNow  (see OnAir.hs)

  - sendOnAirNext

- IO error handling


Could work better
-----------------

- Find the best acceptable value for the longest possible thread lag,
  `timeFrame'. Currently it equals one second. If it gets sufficiently long --
  longer than a second, which may be possible in a heavily overloaded system --
  the current algorithm can miss episodes. Note that if we can allow this value
  to be greater but then notifications may be issued before or after reasonable
  time.

- Continuous integration:

  - Routine unit tests for `timeBeforeSend' and related functions.

  - Test cases to study long term behaviour.

  - Profiling, especially to certify that there are no memory leaks in the
    infinite loop.

- Faster data mining in the XML trees in Schedule.hs.
