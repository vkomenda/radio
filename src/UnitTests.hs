-- | Unit tests for radio schedules.

import Radio.Schedule
import Radio.OnAir

import Test.Tasty
import Test.Tasty.HUnit

import Data.Time.Clock
import Data.Time.Calendar
import Control.Concurrent
import Control.Arrow


main :: IO ()
main = defaultMain tests

tests :: TestTree
tests =
  testGroup "Unit tests"
  [
    testGroup "Correctness of schedule parsing"
    [
      testCase "Schedules are populated" $
      do
        schedules <- mapM getSchedule xmlSchedules
        (schedules /= []) @?= True

      -- TODO: Add more tests in this group, possibly using artificial data.

    ]
  , testGroup "Search functions on schedules"
    [
      testCase "Detect that the given episode is current" $
      do
        now <- getCurrentTime
        episode <- current now
        isCurrentEpisode now episode @?= True
    , testCase "Find 1 next episode to start in 1 minute" $
      do
        now <- getCurrentTime
        episode <- next now 1
        nextEpisodes now 1 [episode] @?= [episode]
    , testCase "A next episode is not current" $
      do
        now <- getCurrentTime
        episode <- next now 1
        isCurrentEpisode now episode @?= False
    , testCase "Two out of three episodes are to be logged next" $
      do
        now <- getCurrentTime
        episode1 <- next now 1
        episode2 <- notNext now 2
        episode3 <- next now 3
        allNextEpisodes now [episode1, episode2, episode3] @?=
          [episode1, episode3]
    ]
  , testGroup "Behaviour of notifications"
    [
      testCase "Notify on a current episode" $
      do
        now <- getCurrentTime
        episode <- current now
        -- action
        sendOnAir [episode]
        threadDelay secondInMicro       -- wait before sending threads finish
        logText <- readFile "now.log"
        -- postcondition check
        afterTimestamp logText @?=
          "[dummyPid1] dummyTitle - dummySubtitle\n"

    , testCase "Notify on a next episode in 2 minutes" $
      do
        now <- getCurrentTime
        episode <- next now 2
        -- precondition check
        nextEpisodes now 2 [episode] @?= [episode]
        -- action
        sendOnAir [episode]
        threadDelay secondInMicro       -- wait before sending threads finish
        nextLogText <- readFile "next.log"
        let (next1, (next2, next3)) =
              ((id *** breakLine . afterTimestamp) .
               breakLine . afterTimestamp) nextLogText
            dummyText = "[dummyPid2] dummyTitle - dummySubtitle"
        -- postcondition check
        next3 @?= "\n"
        next1 @?= dummyText
        next2 @?= dummyText
        nowLogText <- readFile "now.log"
        let (now2, now3) =
              (breakLine . afterTimestamp . snd . breakLine)
              nowLogText
        now3 @?= "\n"
        now2 @?= dummyText
    ]
  , testGroup "British time parsing in the source format"
    [
      testCase "UTC parsing" $
      do
        show (readSourceTime "2007-09-17T02:00:00Z")
        @?=             "Just 2007-09-17 02:00:00 UTC"
    , testCase "BST parsing" $
      do
        show (readSourceTime "2003-09-19T14:00:00+01:00")
        @?=             "Just 2003-09-19 13:00:00 UTC"

    ]
  ]
  where
    afterTimestamp = snd . break (== '[')
    breakLine = break (== '\n')
    current now = do
      let tomorrow = UTCTime (addDays 1 (utctDay now)) (fromInteger 0)
      return $ Episode "dummyPid1" now tomorrow "dummyTitle" "dummySubtitle"
    next now mins = do
      let start = addUTCTime (toEnum $ (mins * minuteInPico) +
                              secondInPico `div` 2) now
          tomorrow = UTCTime (addDays 1 (utctDay now)) (fromInteger 0)
      return $ Episode "dummyPid2" start tomorrow "dummyTitle" "dummySubtitle"
    notNext now mins = do
      let start = addUTCTime (toEnum $ (mins * minuteInPico) +
                              30 * secondInPico +
                              secondInPico `div` 2) now
          tomorrow = UTCTime (addDays 1 (utctDay now)) (fromInteger 0)
      return $ Episode "dummyPid2" start tomorrow "dummyTitle" "dummySubtitle"
