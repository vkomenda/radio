-- | The main module of the radio schedule notification tool.

import Data.Time.Clock
import Data.Time.Calendar
import Data.List
import Control.Concurrent

import Radio.Schedule
import Radio.OnAir

-- | Initially, and then daily at midnight, gets the schedule for the available
-- radio channels and runs an update thread that expires in a day after its
-- initialisation, which is a requirement towards expiration of schedules.
main :: IO ()
main = do
  putStrLn "\n\nDAB text parser and notifier\n\n"
  updateSchedule >>= mainLoop

mainLoop :: Schedule -> IO ()
mainLoop schedule = do
  putStrLn "\nRadio schedule"
  putStrLn   "--------------\n"
  mapM_ (putStrLn . show) schedule
  putStrLn ""
  tid <- forkIO $ sendOnAir schedule
  -- wait until midnight before updating the schedule
  timeToMidnight >>= threadDelay
  newSchedule <- updateSchedule
  -- kill the sending thread before changing to the updated schedule
  killThread tid
  mainLoop newSchedule

-- | Updates the schedule from the network. Re-tries after 10 seconds if the
-- received schedule is empty.
updateSchedule :: IO Schedule
updateSchedule = do
  schedules <- mapM getSchedule xmlSchedules
  let schedule = sortByStartTime $ concat schedules
  if schedule /= []
    then return schedule
    else do
      putStrLn $ "Unsuccessful schedule update. Will retry in " ++
                 show delay ++ " seconds"
      threadDelay $ delay * secondInMicro
      updateSchedule
  where
    sortByStartTime = sortBy (\a b -> startTime a `compare` startTime b)
    delay = 10 :: Int

-- | Time to midnight in microseconds.
timeToMidnight :: IO Int
timeToMidnight = do
  now <- getCurrentTime
  let tomorrow = UTCTime (addDays 1 (utctDay now)) (fromInteger 0)
  return $ diffToMicro $ diffUTCTime tomorrow now
