module Radio.OnAir where

import Data.Time.Clock
import Control.Concurrent

import Radio.Schedule

-- | TODO: Send an update to the hardware.
--
-- Currently logs the pid and the title information into a file, with a time
-- stamp.
sendOnAirNow :: Episode -> IO ()
sendOnAirNow = logEvent "now.log" . formatEpisodeLog

-- | TODO: Send an update to the hardware.
--
-- Currently logs the pid and the title information into a file, with a time
-- stamp.
sendOnAirNext :: Episode -> IO ()
sendOnAirNext = logEvent "next.log" . formatEpisodeLog

formatEpisodeLog :: Episode -> String
formatEpisodeLog e =
  "[" ++ broadcastPid e ++ "] " ++
  episodeTitle e ++ " - " ++ episodeSubtitle e

logEvent :: String -> String -> IO ()
logEvent file str = do
  time <- getCurrentTime
  appendFile file $ show time ++ ": " ++ str ++ ['\n']

-- | Sends updates based on the schedule argument. When an episode starts, it is
-- removed from the schedule. When the schedule is empty, the function
-- terminates.
sendOnAir :: Schedule -> IO ()
sendOnAir schedule = initialTimeCapsule >>= flip onAir schedule >> return ()

-- | A basic synchronisation tool for adjustment to thread wake up delays which
-- are rather common.
data TimeCapsule = TimeCapsule
                   {
                     timeDeposited :: UTCTime
                   , timeOpen      :: UTCTime
                   }

-- | Self-synchronisation of the first invocation of a tail-recursive real-time
-- function.
initialTimeCapsule :: IO TimeCapsule
initialTimeCapsule = do
  now <- getCurrentTime
  return $ TimeCapsule now now

-- | The tail-recursive worker function.
onAir :: TimeCapsule -> Schedule -> IO ()
onAir _   []       = return ()
onAir cap schedule = do
  now <- getCurrentTime
  let episode = head schedule
  if isCurrentEpisode now episode  -- topmost episode is current
    then do
      _ <- forkIO $ sendOnAirNow episode
      beheadSchedule now
    else if now > endTime episode  -- topmost episode has expired
      then do
        beheadSchedule now
      else do                      -- topmost episode is neither cur. nor exp.
        let next = allNextEpisodes thence schedule
        mapM_ (forkIO . sendOnAirNext) next
        let diffThence   = timeBeforeSend thence schedule
            timeNextOpen = now `max` addUTCTime diffThence thence
                           -- FIXME: We introduce the lower limit "now" to
                           -- prevent negative thread delays. When thread lag
                           -- (now - thence) gets sufficiently long, limiting by
                           -- "now" can miss episodes. So, there is a strong
                           -- assumption currently that the lag doesn't increase
                           -- past timeFrameAllowance. However, it might, in a
                           -- heavily overloaded system.
            diff         = diffUTCTime timeNextOpen now

        _ <- forkIO $ do
             mapM_ (putStrLn . (++) "Next <- " . show) next
             putStrLn $ show now ++ " -- suspending for " ++ show diff

        threadDelay $ diffToMicro diff
        onAir (TimeCapsule now timeNextOpen) schedule
  where
    thence = timeOpen cap  -- adjusted time to calculate next episodes
    beheadSchedule now = onAir (TimeCapsule now thence) $ tail schedule


diffToMicro :: NominalDiffTime -> Int
diffToMicro = (`div` secondInMicro) . fromEnum

-- | Checks whether the given time is within the episode's start and end times.
isCurrentEpisode :: UTCTime -> Episode -> Bool
isCurrentEpisode now episode =
  now >= startTime episode && now < endTime episode

-- | A second expressed in picoseconds.
--
-- CAUTION: base units are picoseconds because of NominalDiffTime; and not
-- seconds as in Int conversions from DiffTime.
secondInPico :: Int
secondInPico = 10^(12::Int)

minuteInPico :: Int
minuteInPico = 60 * secondInPico

secondInMicro :: Int
secondInMicro = 10^(6::Int)

-- | The interval in picoseconds in which to seach for next episodes after
-- predefined time intervals.
timeFrame :: Int
timeFrame = secondInPico

-- | Filters out the "next" episodes within a reasonably short period from 1, 2,
-- 3, 5 and 10 minute boundaries.
allNextEpisodes :: UTCTime -> Schedule -> Schedule
allNextEpisodes now schedule =
  concatMap after [1, 2, 3, 5, 10]
  where
    after m = nextEpisodes now m schedule

-- | Returns the episodes that are to follow in @mins@ minutes from @now@, or
-- very shortly thereafter.
nextEpisodes :: UTCTime -> Int -> Schedule -> Schedule
nextEpisodes now mins =
  allEpisodesBetween now m (m + timeFrame)
  where
    m = mins * minuteInPico

-- | All episodes to be aired within a designated time interval expressed in
-- munutes from now.
allEpisodesBetweenMinutes :: UTCTime -> Int -> Int -> Schedule -> Schedule
allEpisodesBetweenMinutes now lower upper =
  allEpisodesBetween now (lower * minuteInPico) (upper * minuteInPico)

-- | All episodes to be aired within a designated time interval expressed in
-- picoseconds from now.
allEpisodesBetween :: UTCTime -> Int -> Int -> Schedule -> Schedule
allEpisodesBetween now lower upper schedule = do
  filter (\e ->
           let timeRemain = timeToStart now e in
           lower <= timeRemain &&
           upper >  timeRemain)
         schedule

-- | Calculates the time to wait before sending an update.
--
--   CAUTION: in picoseconds.
timeBeforeSend :: UTCTime -> Schedule -> NominalDiffTime
timeBeforeSend now schedule = toEnum $ minimumDefault timeFrame next
  where
    next =
      filter (0 <=)
      (
         map                   (timeToStart now) (0  `between` 1)
      ++ map (minusMinutes 1  . timeToStart now) (1  `between` 2)
      ++ map (minusMinutes 2  . timeToStart now) (2  `between` 3)
      ++ map (minusMinutes 3  . timeToStart now) (3  `between` 4)
      ++ map (minusMinutes 3  . timeToStart now) (4  `between` 5)
      ++ map (minusMinutes 5  . timeToStart now) (5  `between` 6)
      ++ map (minusMinutes 5  . timeToStart now) (6  `between` 10)
      ++ map (minusMinutes 10 . timeToStart now) (10 `between` dayInMins)
      )

    between m n =
      allEpisodesBetween now
                         (m * minuteInPico + timeFrame)
                         (n * minuteInPico + timeFrame)
                         schedule
    minusMinutes m t = t - m * minuteInPico
    dayInMins = 24 * 60

-- | Time to start, in picoseconds.
timeToStart :: UTCTime -> Episode -> Int
timeToStart now e = fromEnum $ diffUTCTime (startTime e) now

-- | A minimum computation for a list that is allowed to be empty, in which case
-- the minimum is the given default value.
minimumDefault :: Ord a => a -> [a] -> a
minimumDefault d [] = d
minimumDefault _ xs = minimum xs
