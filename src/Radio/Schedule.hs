-- | Schedule download and parsing facility.

module Radio.Schedule where

import Text.HandsomeSoup
import Text.XML.HXT.Core
-- import Text.Pandoc
import Data.Time.Clock
import Data.Time.Format
import System.Locale
import Data.Maybe
import Data.List

-- | A data type capturing the essential information about an episode.
data Episode = Episode
               {
                 broadcastPid    :: String
               , startTime       :: UTCTime
               , endTime         :: UTCTime
               , episodeTitle    :: String
               , episodeSubtitle :: String
               }
             deriving (Eq, Ord)

instance Show Episode where
  show (Episode p s e t st) =
    "[" ++ p ++ "]: " ++ show s ++ " -- " ++
    show e ++ " / " ++ t ++ " - " ++ st

type Schedule = [Episode]

episodeFromTuple :: (String, UTCTime, UTCTime, String, String) -> Episode
episodeFromTuple (p, s, e, t, st) = Episode p s e t st

-- | All the known schedules.
xmlSchedules :: [String]
xmlSchedules =
  [
    "http://www.bbc.co.uk/radio2/programmes/schedules.xml"
  , "http://www.bbc.co.uk/5livesportsextra/programmes/schedules.xml"
  , "http://www.bbc.co.uk/worldserviceradio/programmes/schedules.xml"
  ]

-- | Gets the schedule from a given URL.
getSchedule :: String -> IO Schedule
getSchedule url = do
  let doc = fromUrl url
{-
  This may be useful for debug purposes:

  res <- runX . xshow $ doc
  mapM_ putStrLn res
  putStrLn "Pandoc conversions"
  let pandoc = readHtml def $ unlines res
  putStrLn $ writeMarkdown def pandoc
-}
--  putStrLn "========"
  pids <- runX $ doc >>> css "broadcast" /> hasName "pid" /> getText
--  mapM_ print pids
--  putStrLn "========"
  starts0 <- runX $ doc >>> css "broadcast" /> hasName "start" /> getText
  let starts = map (fromJust . readSourceTime) starts0
--  mapM_ print starts
--  putStrLn "========"
  ends0 <- runX $ doc >>> css "broadcast" /> hasName "end" /> getText
  let ends = map (fromJust . readSourceTime) ends0
--  mapM_ print ends
--  putStrLn "========"
  titles <- runX $ doc >>> css "broadcast" />
            hasName "programme" /> hasName "display_titles" />
            hasName "title" /> getText
--  mapM_ print titles
--  putStrLn "========"
  subtitles <- runX $ doc >>> css "broadcast" />
               hasName "programme" /> hasName "display_titles" />
               hasName "subtitle" /> getText
--  mapM_ print subtitles
  let schedule = map episodeFromTuple $
                 zip5 pids starts ends titles subtitles
  return schedule

-- | Reads time from a provided string.
--
--   TODO: Consider output in local time (UTC/BST) because the source data is
--   limited to British time only. However, UTC alone works just fine.
readSourceTime :: String -> Maybe UTCTime
readSourceTime raw
  | utc /= Nothing = utc
  | otherwise      = nonUtc
  where
    utc       = parse fmtUTC
    nonUtc    = parse fmtOffset
    fmtUTC    = "%FT%TZ"
    fmtOffset = "%FT%T%z"
    parse fmt = parseTime defaultTimeLocale fmt raw
