DAB Radio Schedule Notifier Tool
================================


Satisfied requirements
----------------------

1. The application shall support the Radio 2, Radio 5 Live Sports Extra and
World Service BBC services.

2. The process may be started at any point during the day, and if so, should
correctly populate itself.

3. An "On Air Now" DAB text message shall be sent when each broadcasts starts,
i.e., if it was 17:55, and the schedule contained the 6 O’clock news, it was
send a message in five minutes time.

4. Each schedule page on the web covers 1 day, the program should be able to
refresh itself to cover subsequent days.

5. An "On Air Next" DAB text message shall be sent at 10, 5, 3, 2, and 1 minute
before each broadcast.



Installation
------------

Please run

> cabal install

in the directory where `radio.cabal' is located.


Tests
-----

To compile and run unit tests, there is a special option in cabal:

> cabal configure --enable-tests
>
> cabal build

If you have log files from previous tests in the working directory, don't forget
to remove them. Then run the tests:

> ./dist/build/Radio-unit-tests/Radio-unit-tests

Don't worry about a short delay during the "Notify on a next episode in 2
minutes" test. It is intended to test a "real-world" behaviour. After the tests
have finished, you will find two event log files in the working directory, with
the content similar to the one below:

> $ cat next.log
>
> 2014-01-30 11:01:05.875517 UTC: [dummyPid2] dummyTitle - dummySubtitle
>
> 2014-01-30 11:02:05.959372 UTC: [dummyPid2] dummyTitle - dummySubtitle

> $ cat now.log
>
> 2014-01-30 11:01:04.867735 UTC: [dummyPid1] dummyTitle - dummySubtitle
>
> 2014-01-30 11:03:06.915889 UTC: [dummyPid2] dummyTitle - dummySubtitle

which means that the "dummyPid1" program starts first, and it is already on air
at the time of sending the first update. The "dummyPid2" program is to be aired
in 2 minutes at the time of the first update, which is logged. After one minute,
"dummyPid2" is logged again as a next episode. Finally, it is aired after two
minutes minute, which is logged as "now on air".
